\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Prinzipielle Arbeitsweise eines Bootloaders}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Die Hardware der AVR 8-Bit Mikrocontroller}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}CPU und Speicherzugriff}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Eingabe und Ausgabefunktion}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Das Starten des AVR Mikrocontrollers}{9}{section.2.3}
\contentsline {section}{\numberline {2.4}Das Beschreiben des AVR Speichers}{11}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}parallele Programmierung}{12}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}serieller Download mit ISP}{12}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Selbstprogrammierung mit serieller Schnittstelle}{13}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Diagnose Werkzeuge}{14}{subsection.2.4.4}
\contentsline {chapter}{\numberline {3}Der optiboot Bootloader f\IeC {\"u}r AVR Mikrocontroller}{16}{chapter.3}
\contentsline {section}{\numberline {3.1}\IeC {\"A}nderungen und Weiterentwicklung von Version 6.2}{16}{section.3.1}
\contentsline {section}{\numberline {3.2}Eigenschaften der Assemblerversion von optiboot}{17}{section.3.2}
\contentsline {section}{\numberline {3.3}Automatische Gr\IeC {\"o}\IeC {\ss }enanpassung in der optiboot Makefile}{19}{section.3.3}
\contentsline {section}{\numberline {3.4}Zielvorgaben f\IeC {\"u}r optiboot Makefile}{20}{section.3.4}
\contentsline {section}{\numberline {3.5}Die Optionen f\IeC {\"u}r die optiboot Makefile}{21}{section.3.5}
\contentsline {section}{\numberline {3.6}Benutzung von optiboot ohne Bootloader-Bereich}{25}{section.3.6}
\contentsline {section}{\numberline {3.7}Die M\IeC {\"o}glichkeiten der seriellen Schnittstelle mit der verwendeten Software}{27}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Berechnung der Verz\IeC {\"o}gerungszeit}{29}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Benutzung von mehr als einer seriellen Schnittstelle}{30}{subsection.3.7.2}
\contentsline {subsection}{\numberline {3.7.3}Serielle Eingabe und Ausgabe \IeC {\"u}ber nur einen AVR Pin}{31}{subsection.3.7.3}
\contentsline {subsection}{\numberline {3.7.4}Benutzung der automatischen Baudraten-Bestimmung}{33}{subsection.3.7.4}
\contentsline {section}{\numberline {3.8}Einige Beispiele f\IeC {\"u}r die Erzeugung eines optiboot Bootloaders}{39}{section.3.8}
\contentsline {section}{\numberline {3.9}Anpassung der Taktfrequenz bei internem RC-Generator}{42}{section.3.9}
\contentsline {subsection}{\numberline {3.9.1}Untersuchung der RC-Generatoren des ATmega8}{43}{subsection.3.9.1}
\contentsline {subsection}{\numberline {3.9.2}Untersuchung der RC-Generatoren des ATmega8535}{43}{subsection.3.9.2}
\contentsline {subsection}{\numberline {3.9.3}Untersuchung der RC-Generatoren des ATmega8515 und des ATmega162}{44}{subsection.3.9.3}
\contentsline {subsection}{\numberline {3.9.4}Untersuchung der RC-Generatoren der ATmega328 Familie}{44}{subsection.3.9.4}
\contentsline {subsection}{\numberline {3.9.5}Untersuchung der RC-Generatoren des ATmega32 / 16}{45}{subsection.3.9.5}
\contentsline {subsection}{\numberline {3.9.6}Untersuchung des RC-Generators des ATmega163L}{46}{subsection.3.9.6}
\contentsline {subsection}{\numberline {3.9.7}Untersuchung der RC-Generatoren des ATmega64 / 128}{46}{subsection.3.9.7}
\contentsline {subsection}{\numberline {3.9.8}Untersuchung der RC-Generatoren der ATmega644 Familie}{47}{subsection.3.9.8}
\contentsline {subsection}{\numberline {3.9.9}Untersuchung der RC-Generatoren der ATmega645 Familie}{47}{subsection.3.9.9}
\contentsline {subsection}{\numberline {3.9.10}Untersuchung der RC-Generatoren der ATmega649 Familie}{47}{subsection.3.9.10}
\contentsline {subsection}{\numberline {3.9.11}Untersuchung des RC-Generators der ATmega2560 Familie}{48}{subsection.3.9.11}
\contentsline {subsection}{\numberline {3.9.12}Untersuchung der RC-Generatoren der ATtiny4313 Familie}{48}{subsection.3.9.12}
\contentsline {subsection}{\numberline {3.9.13}Untersuchung der RC-Generatoren der ATtiny84 Familie}{49}{subsection.3.9.13}
\contentsline {subsection}{\numberline {3.9.14}Untersuchung der RC-Generatoren der ATtiny85 Familie}{49}{subsection.3.9.14}
\contentsline {subsection}{\numberline {3.9.15}Untersuchung der RC-Generatoren der ATtiny841 Familie}{50}{subsection.3.9.15}
\contentsline {subsection}{\numberline {3.9.16}Untersuchung der RC-Generatoren der ATtiny861 Familie}{51}{subsection.3.9.16}
\contentsline {subsection}{\numberline {3.9.17}Untersuchung des RC-Generators der ATtiny87 Familie}{51}{subsection.3.9.17}
\contentsline {subsection}{\numberline {3.9.18}Untersuchung des RC-Generators der ATtiny88 Familie}{51}{subsection.3.9.18}
\contentsline {subsection}{\numberline {3.9.19}Untersuchung der RC-Generatoren des ATtiny1634}{52}{subsection.3.9.19}
\contentsline {subsection}{\numberline {3.9.20}Untersuchung des RC-Generators der AT90PWM Familie}{52}{subsection.3.9.20}
\contentsline {subsection}{\numberline {3.9.21}Untersuchung des RC-Generators der AT90CAN Familie}{52}{subsection.3.9.21}
\contentsline {chapter}{\numberline {4}Daten der AVR 8-Bit Mikrocontroller}{54}{chapter.4}
\contentsline {section}{\numberline {4.1}Signatur Bytes und Standard Fuse Einstellung}{54}{section.4.1}
\contentsline {section}{\numberline {4.2}Belegung der Fuses}{57}{section.4.2}
\contentsline {section}{\numberline {4.3}M\IeC {\"o}gliche Interne Takt-Frequenzen}{60}{section.4.3}
\contentsline {chapter}{\numberline {5}Verschiedene USB zu Seriell Wandler mit Linux}{61}{chapter.5}
\contentsline {section}{\numberline {5.1}Der CH340G und der CP2102 Wandler }{61}{section.5.1}
\contentsline {section}{\numberline {5.2}Der PL-2303 und der FT232R Wandler}{64}{section.5.2}
\contentsline {section}{\numberline {5.3}Der USB-serial Wandler mit der ATmega16X2 Software}{67}{section.5.3}
\contentsline {section}{\numberline {5.4}Der Pololu USB AVR Programmer v2.1}{69}{section.5.4}
