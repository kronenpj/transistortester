\BOOKMARK [0][-]{chapter.1}{Prinzipielle Arbeitsweise eines Bootloaders}{}% 1
\BOOKMARK [0][-]{chapter.2}{Die Hardware der AVR 8-Bit Mikrocontroller}{}% 2
\BOOKMARK [1][-]{section.2.1}{CPU und Speicherzugriff}{chapter.2}% 3
\BOOKMARK [1][-]{section.2.2}{Eingabe und Ausgabefunktion}{chapter.2}% 4
\BOOKMARK [1][-]{section.2.3}{Das Starten des AVR Mikrocontrollers}{chapter.2}% 5
\BOOKMARK [1][-]{section.2.4}{Das Beschreiben des AVR Speichers}{chapter.2}% 6
\BOOKMARK [2][-]{subsection.2.4.1}{parallele Programmierung}{section.2.4}% 7
\BOOKMARK [2][-]{subsection.2.4.2}{serieller Download mit ISP}{section.2.4}% 8
\BOOKMARK [2][-]{subsection.2.4.3}{Selbstprogrammierung mit serieller Schnittstelle}{section.2.4}% 9
\BOOKMARK [2][-]{subsection.2.4.4}{Diagnose Werkzeuge}{section.2.4}% 10
\BOOKMARK [0][-]{chapter.3}{Der optiboot Bootloader f\374r AVR Mikrocontroller}{}% 11
\BOOKMARK [1][-]{section.3.1}{\304nderungen und Weiterentwicklung von Version 6.2}{chapter.3}% 12
\BOOKMARK [1][-]{section.3.2}{Eigenschaften der Assemblerversion von optiboot}{chapter.3}% 13
\BOOKMARK [1][-]{section.3.3}{Automatische Gr\366\337enanpassung in der optiboot Makefile}{chapter.3}% 14
\BOOKMARK [1][-]{section.3.4}{Zielvorgaben f\374r optiboot Makefile}{chapter.3}% 15
\BOOKMARK [1][-]{section.3.5}{Die Optionen f\374r die optiboot Makefile}{chapter.3}% 16
\BOOKMARK [1][-]{section.3.6}{Benutzung von optiboot ohne Bootloader-Bereich}{chapter.3}% 17
\BOOKMARK [1][-]{section.3.7}{Die M\366glichkeiten der seriellen Schnittstelle mit der verwendeten Software}{chapter.3}% 18
\BOOKMARK [2][-]{subsection.3.7.1}{Berechnung der Verz\366gerungszeit}{section.3.7}% 19
\BOOKMARK [2][-]{subsection.3.7.2}{Benutzung von mehr als einer seriellen Schnittstelle}{section.3.7}% 20
\BOOKMARK [2][-]{subsection.3.7.3}{Serielle Eingabe und Ausgabe \374ber nur einen AVR Pin}{section.3.7}% 21
\BOOKMARK [2][-]{subsection.3.7.4}{Benutzung der automatischen Baudraten-Bestimmung}{section.3.7}% 22
\BOOKMARK [1][-]{section.3.8}{Einige Beispiele f\374r die Erzeugung eines optiboot Bootloaders}{chapter.3}% 23
\BOOKMARK [1][-]{section.3.9}{Anpassung der Taktfrequenz bei internem RC-Generator}{chapter.3}% 24
\BOOKMARK [2][-]{subsection.3.9.1}{Untersuchung der RC-Generatoren des ATmega8}{section.3.9}% 25
\BOOKMARK [2][-]{subsection.3.9.2}{Untersuchung der RC-Generatoren des ATmega8535}{section.3.9}% 26
\BOOKMARK [2][-]{subsection.3.9.3}{Untersuchung der RC-Generatoren des ATmega8515 und des ATmega162}{section.3.9}% 27
\BOOKMARK [2][-]{subsection.3.9.4}{Untersuchung der RC-Generatoren der ATmega328 Familie}{section.3.9}% 28
\BOOKMARK [2][-]{subsection.3.9.5}{Untersuchung der RC-Generatoren des ATmega32 / 16}{section.3.9}% 29
\BOOKMARK [2][-]{subsection.3.9.6}{Untersuchung des RC-Generators des ATmega163L}{section.3.9}% 30
\BOOKMARK [2][-]{subsection.3.9.7}{Untersuchung der RC-Generatoren des ATmega64 / 128}{section.3.9}% 31
\BOOKMARK [2][-]{subsection.3.9.8}{Untersuchung der RC-Generatoren der ATmega644 Familie}{section.3.9}% 32
\BOOKMARK [2][-]{subsection.3.9.9}{Untersuchung der RC-Generatoren der ATmega645 Familie}{section.3.9}% 33
\BOOKMARK [2][-]{subsection.3.9.10}{Untersuchung der RC-Generatoren der ATmega649 Familie}{section.3.9}% 34
\BOOKMARK [2][-]{subsection.3.9.11}{Untersuchung des RC-Generators der ATmega2560 Familie}{section.3.9}% 35
\BOOKMARK [2][-]{subsection.3.9.12}{Untersuchung der RC-Generatoren der ATtiny4313 Familie}{section.3.9}% 36
\BOOKMARK [2][-]{subsection.3.9.13}{Untersuchung der RC-Generatoren der ATtiny84 Familie}{section.3.9}% 37
\BOOKMARK [2][-]{subsection.3.9.14}{Untersuchung der RC-Generatoren der ATtiny85 Familie}{section.3.9}% 38
\BOOKMARK [2][-]{subsection.3.9.15}{Untersuchung der RC-Generatoren der ATtiny841 Familie}{section.3.9}% 39
\BOOKMARK [2][-]{subsection.3.9.16}{Untersuchung der RC-Generatoren der ATtiny861 Familie}{section.3.9}% 40
\BOOKMARK [2][-]{subsection.3.9.17}{Untersuchung des RC-Generators der ATtiny87 Familie}{section.3.9}% 41
\BOOKMARK [2][-]{subsection.3.9.18}{Untersuchung des RC-Generators der ATtiny88 Familie}{section.3.9}% 42
\BOOKMARK [2][-]{subsection.3.9.19}{Untersuchung der RC-Generatoren des ATtiny1634}{section.3.9}% 43
\BOOKMARK [2][-]{subsection.3.9.20}{Untersuchung des RC-Generators der AT90PWM Familie}{section.3.9}% 44
\BOOKMARK [2][-]{subsection.3.9.21}{Untersuchung des RC-Generators der AT90CAN Familie}{section.3.9}% 45
\BOOKMARK [0][-]{chapter.4}{Daten der AVR 8-Bit Mikrocontroller}{}% 46
\BOOKMARK [1][-]{section.4.1}{Signatur Bytes und Standard Fuse Einstellung}{chapter.4}% 47
\BOOKMARK [1][-]{section.4.2}{Belegung der Fuses}{chapter.4}% 48
\BOOKMARK [1][-]{section.4.3}{M\366gliche Interne Takt-Frequenzen}{chapter.4}% 49
\BOOKMARK [0][-]{chapter.5}{Verschiedene USB zu Seriell Wandler mit Linux}{}% 50
\BOOKMARK [1][-]{section.5.1}{Der CH340G und der CP2102 Wandler }{chapter.5}% 51
\BOOKMARK [1][-]{section.5.2}{Der PL-2303 und der FT232R Wandler}{chapter.5}% 52
\BOOKMARK [1][-]{section.5.3}{Der USB-serial Wandler mit der ATmega16X2 Software}{chapter.5}% 53
\BOOKMARK [1][-]{section.5.4}{Der Pololu USB AVR Programmer v2.1}{chapter.5}% 54
