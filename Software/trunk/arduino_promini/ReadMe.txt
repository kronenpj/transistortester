You must setup a shield board for the Arduino ProMini to use the transistortester program.

My suggestion is https://gitlab.com/kronenpj/TransistorTester-ProMini. But you are, of course, free
to design your own.

On my Linux system I have used the Arduino sketch environment to select the board (Tools/Board -> Arduino Pro Mini).
Then I could check and select the right serial port (Tools/Serial Port -> /dev/ttyUSB0).
Please check the number of the port (ttyUSB0, ttyUSB0 ...).

Your transistortester Makefile in the directory trunk/arduino_promini should use the same
USB PORT (behind PROGRAMMER=arduino).

Please do not use the arduino GUI / sketch to build a transistortester program for the Arduino.
You should use the included Makefile with the avr-gcc and the avrdude to build the program data.

If you use the "make upload" command with the 'arduino' programmer, the program data is loaded
correctly, but the EEprom data is not. Using a 'real' SPL programmer, like a stk500v1 or buspirate,
works well.

