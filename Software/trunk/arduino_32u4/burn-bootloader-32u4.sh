#!/usr/bin/env bash

PORT=/dev/ttyUSB0
PRG=buspirate

DUDEPATH=/opt/arduino-1.8.12/hardware/tools/avr
DUDECFG=/opt/arduino-1.8.12/hardware/tools/avr/etc

${DUDEPATH}/bin/avrdude -C${DUDEPATH}/etc/avrdude.conf -v -patmega32u4 -c${PRG} -P${PORT} -e -Ulock:w:0x3F:m -Uefuse:w:0xcb:m -Uhfuse:w:0xd8:m -Ulfuse:w:0xff:m

# Arduino default bootloader.
#${DUDEPATH}/bin/avrdude -C${DUDEPATH}/etc/avrdude.conf -v -patmega32u4 -c${PRG} -P${PORT} -Uflash:w:/home/kronenpj/.arduino15/packages/adafruit/hardware/avr/1.4.13/bootloaders/caterina/Caterina-Atmega32u4.hex:i -Ulock:w:0x2F:m 

# Adafruit bootloader
${DUDEPATH}/bin/avrdude -C/opt/arduino-1.8.12/hardware/tools/avr/etc/avrdude.conf -v -patmega32u4 -c${PRG} -P${PORT} -Uflash:w:/home/kronenpj/projects/circuits/datasheets/bootloaderCDC-32u4.hex:i -Ulock:w:0x2F:m 

