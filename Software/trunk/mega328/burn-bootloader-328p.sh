#!/usr/bin/env bash

BITCLOCK="-B 20"
BITCLOCK=""
PORT=/dev/ttyUSB0
PRG=buspirate
LFUSE=0xe2
HFUSE=0xdc
EFUSE=0xfd
ULOCK=0x3f
LOCK=0x0f

DUDEPATH=/opt/arduino-1.8.12/hardware/tools/avr

${DUDEPATH}/bin/avrdude -C${DUDEPATH}/etc/avrdude.conf -v -patmega328p -c${PRG} -P${PORT} ${BITCLOCK} -e -Ulock:w:${ULOCK}:m -Uefuse:w:${EFUSE}:m -Uhfuse:w:${HFUSE}:m -Ulfuse:w:${LFUSE}:m

[ $? != 0 ] && exit

# Arduino default bootloader.
#${DUDEPATH}/bin/avrdude -C${DUDEPATH}/etc/avrdude.conf -v -patmega32u4 -c${PRG} -P${PORT} -Uflash:w:/home/kronenpj/.arduino15/packages/adafruit/hardware/avr/1.4.13/bootloaders/caterina/Caterina-Atmega32u4.hex:i -Ulock:w:0x2F:m 

# Adafruit bootloader
${DUDEPATH}/bin/avrdude -C${DUDEPATH}/etc/avrdude.conf -v -c ${PRG} -patmega328p -P${PORT} ${BITCLOCK} -Uflash:w:/home/kronenpj/src/svn/optiboot/optiboot/bootloaders/optiboot/optiboot_atmega328_384_8.elf -Ulock:w:${LOCK}:m
